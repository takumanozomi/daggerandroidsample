package com.example.takuma_n.daggerapp.simple;

public interface Heater {
    void on();
    void off();
}
