package com.example.takuma_n.daggerapp.singleton;

import android.util.Log;

public class SingletonInstance {

    public void call() {
        Log.d("DaggerApp", "Singleton instance: " + String.valueOf(hashCode()));
    }
}
