package com.example.takuma_n.daggerapp.simple;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.takuma_n.daggerapp.R;

import javax.inject.Inject;

public class SimpleDiActivity extends AppCompatActivity{

    @Inject CoffeeMaker maker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerCoffeeShop.create().inject(this);
        maker.brew();
        DaggerCoffeeShop.create().maker().brew();
    }
}
