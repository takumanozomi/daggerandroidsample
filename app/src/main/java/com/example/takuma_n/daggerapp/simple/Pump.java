package com.example.takuma_n.daggerapp.simple;

public interface Pump {
    void pump();
}
