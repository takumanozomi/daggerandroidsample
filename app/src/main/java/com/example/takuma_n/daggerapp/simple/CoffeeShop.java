package com.example.takuma_n.daggerapp.simple;

import dagger.Component;

@Component(modules = DripCoffeeModule.class)
public interface CoffeeShop {

    CoffeeMaker maker();

    void inject(SimpleDiActivity activity);
}
