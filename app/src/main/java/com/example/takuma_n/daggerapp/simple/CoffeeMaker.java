package com.example.takuma_n.daggerapp.simple;

import android.util.Log;

import javax.inject.Inject;

public class CoffeeMaker {
    @Inject
    Heater heater;
    @Inject
    Pump pump;

    @Inject CoffeeMaker() {
    }

    public void brew() {
        heater.on();
        pump.pump();
        Log.d("CoffeeMaker", "<<<< brewed >>>>");
        heater.off();
    }
}
