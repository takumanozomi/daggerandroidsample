package com.example.takuma_n.daggerapp.singleton;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.takuma_n.daggerapp.R;
import com.example.takuma_n.daggerapp.context.ContextModule;
import com.example.takuma_n.daggerapp.context.DataStore;

import javax.inject.Inject;

public class SingletonInjectActivity extends AppCompatActivity {

    @Inject SingletonInstance singletonInstance;
    @Inject DataStore dataStore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MyApplication)getApplication())
                .component
                .subActivityComponentBuilder()
                .requestModule(new ContextModule(this))
                .build().inject(this);

        singletonInstance.call();
        dataStore.store("SingletonInjectActivity");
    }
}
