package com.example.takuma_n.daggerapp.context;

import dagger.Component;

@Component(modules = {ContextModule.class})
public interface ActivityComponent {

    void inject(ContextInjectActivity activity);
}
