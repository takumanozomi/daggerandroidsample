package com.example.takuma_n.daggerapp.singleton;

import com.example.takuma_n.daggerapp.ChildScope;
import com.example.takuma_n.daggerapp.context.ContextModule;

import dagger.Subcomponent;

@ChildScope
@Subcomponent(modules = ContextModule.class)
public interface SubActivityComponent {

    @Subcomponent.Builder
    interface Builder {
        Builder requestModule(ContextModule module);
        SubActivityComponent build();
    }

    void inject(SingletonInjectActivity activity);
}
