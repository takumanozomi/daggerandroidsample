package com.example.takuma_n.daggerapp.androidsupport;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

@Module(subcomponents = AndroidSupportActivityComponent.class)
public abstract class AndroidSupportActivityModule {
    @Binds @IntoMap @ActivityKey(AndroidSupportActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> bindYourActivityInjectorFactory(AndroidSupportActivityComponent.Builder builder);
}
