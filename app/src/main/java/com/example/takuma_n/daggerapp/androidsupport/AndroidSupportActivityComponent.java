package com.example.takuma_n.daggerapp.androidsupport;

import com.example.takuma_n.daggerapp.context.ContextModule;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

@Subcomponent(modules = ContextModule.class)
public interface AndroidSupportActivityComponent extends AndroidInjector<AndroidSupportActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<AndroidSupportActivity> {

        public abstract Builder contextModule(ContextModule contextModule);

        @Override
        public void seedInstance(AndroidSupportActivity instance) {
            contextModule(new ContextModule(instance));
        }
    }
}
