package com.example.takuma_n.daggerapp.simple;

import dagger.Module;
import dagger.Provides;

@Module
public class DripCoffeeModule {

    @Provides static Heater provideHeater() {
        return new ElectricHeater();
    }

    @Provides static Pump providePump(Thermosiphon pump) {
        return pump;
    }
}
