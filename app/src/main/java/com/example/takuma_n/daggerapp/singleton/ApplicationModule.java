package com.example.takuma_n.daggerapp.singleton;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(subcomponents = SubActivityComponent.class)
public class ApplicationModule {

    @Provides
    @Singleton SingletonInstance provideSingletonInstance() {
        return new SingletonInstance();
    }
}
