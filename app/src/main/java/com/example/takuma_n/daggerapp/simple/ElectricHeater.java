package com.example.takuma_n.daggerapp.simple;

import android.util.Log;

import com.example.takuma_n.daggerapp.simple.Heater;

public class ElectricHeater implements Heater {

    @Override public void on() {
        Log.d("ElectricHeater", "~~~~ heat on ~~~~");
    }

    @Override public void off() {
        Log.d("ElectricHeater", "~~~~ heat off ~~~~");
    }
}
