package com.example.takuma_n.daggerapp.context;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import javax.inject.Inject;

public class DataStore {

    Context context;

    @Inject public DataStore(Context context) {
        this.context = context;
    }

    public void store(String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("Dagger", Context.MODE_PRIVATE).edit();
        editor.putString("Value", value);
        editor.apply();
        Log.d("DaggerApp", "DataStore instance" + String.valueOf(hashCode()));
    }
}
