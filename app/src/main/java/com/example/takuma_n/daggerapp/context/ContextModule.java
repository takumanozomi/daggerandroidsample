package com.example.takuma_n.daggerapp.context;

import android.content.Context;
import android.util.Log;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    Context context;

    public ContextModule (Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext(){
        Log.d("DaggerApp", "provide " + context.getClass().getSimpleName());
        return this.context;
    }
}
