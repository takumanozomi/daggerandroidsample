package com.example.takuma_n.daggerapp.singleton;

import com.example.takuma_n.daggerapp.androidsupport.AndroidSupportActivityModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {ApplicationModule.class, AndroidSupportActivityModule.class})
public interface ApplicationComponent extends AndroidInjector<MyApplication> {
    SingletonInstance singletonInstance();

    SubActivityComponent.Builder subActivityComponentBuilder();
}
