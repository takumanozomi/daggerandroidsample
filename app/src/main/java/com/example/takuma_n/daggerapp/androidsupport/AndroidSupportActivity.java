package com.example.takuma_n.daggerapp.androidsupport;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.takuma_n.daggerapp.context.DataStore;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class AndroidSupportActivity extends AppCompatActivity {

    @Inject
    DataStore dataStore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        dataStore.store("AndroidSupportActivity");
    }
}
