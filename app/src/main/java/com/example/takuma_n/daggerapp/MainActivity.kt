package com.example.takuma_n.daggerapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.takuma_n.daggerapp.androidsupport.AndroidSupportActivity
import com.example.takuma_n.daggerapp.context.ContextInjectActivity
import com.example.takuma_n.daggerapp.simple.SimpleDiActivity
import com.example.takuma_n.daggerapp.singleton.SingletonInjectActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        simple_inject.setOnClickListener{ _ -> startActivity(Intent(this, SimpleDiActivity::class.java))}
        context_inject.setOnClickListener{ _ -> startActivity(Intent(this, ContextInjectActivity::class.java))}
        singleton_inject.setOnClickListener{ _ -> startActivity(Intent(this, SingletonInjectActivity::class.java))}
        android_support.setOnClickListener{ _ -> startActivity(Intent(this, AndroidSupportActivity::class.java))}
    }
}
