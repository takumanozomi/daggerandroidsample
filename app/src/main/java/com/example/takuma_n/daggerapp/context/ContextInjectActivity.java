package com.example.takuma_n.daggerapp.context;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

public class ContextInjectActivity extends AppCompatActivity {
    @Inject DataStore dataStore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .contextModule(new ContextModule(this))
                .build().inject(this);

        dataStore.store("Inject Success");
    }
}
